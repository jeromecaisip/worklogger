import React, {Component} from 'react';
import LogForm from './components/LogForm'
import {DailySummary} from "./components/DailySummary";
import {WorkLogs} from "./components/WorkLogs";
import SortingMenu from './components/SortingMenu';
import ShowMore from './components/ShowMore';
import {DEFAULT, DRAGON_MY, DRAGON_P2P, DURATION, DURATION_REVERSE, HYDRA, PROJECT, PROJECT_REVERSE} from "./constants";

const PROJECT_LIST = [HYDRA, DRAGON_MY, DRAGON_P2P];


function createTimeLog(id, duration, project, remarks, date) {
    let dateobj = new Date();
    return {
        id,
        duration,
        project,
        remarks,
        date: dateobj
    }
}


function getLargestId(items) {
    let maxId = items.reduce(function (maxId, item) {
        return Math.max(maxId, item.id)
    }, 0);
    return maxId
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            worklogs: [],
            list_size: 4,
            sorting: DEFAULT,
            unapply_on_sort: true,
        };
        this.createWorkLog = this.createWorkLog.bind(this);
        this.deleteWorkLog = this.deleteWorkLog.bind(this);
        this.editWorkLog = this.editWorkLog.bind(this);
        this.getTotalDurationToday = this.getTotalDurationToday.bind(this);
        this.getProjectDurationAggregate = this.getProjectDurationAggregate.bind(this);
        this.showMore = this.showMore.bind(this);
        this.sortWorklogs = this.sortWorklogs.bind(this);
    }

    createWorkLog(duration, project, remarks) {
        let largestId = getLargestId([...this.state.worklogs]);
        let new_worklog = createTimeLog(largestId + 1, Number(duration), project, remarks);
        let worklogs = [...this.state.worklogs, new_worklog];
        this.setState({worklogs: worklogs})
    }

    sortWorklogs(type) {
        let worklogs = [...this.state.worklogs];
        switch (type) {
            case DEFAULT:
                worklogs.sort(function (a, b) {
                    return a.date - b.date
                });
                break;
            case DURATION:
                worklogs.sort(function (a, b) {
                    return a.duration - b.duration
                });
                break;
            case DURATION_REVERSE:
                worklogs.sort(function (a, b) {
                    return b.duration - a.duration
                });
                break;
            //FIXME: Project sorting
            case PROJECT:
                worklogs.sort(function (a, b) {
                    if (a.project < b.project) {
                        return -1
                    } else {
                        return 1
                    }
                });
                break;
            case PROJECT_REVERSE:
                worklogs.sort(function (a, b) {
                    if (b.project < a.project) {
                        return -1
                    } else {
                        return 1
                    }
                });
                break;
            default:
                worklogs.sort(function (a, b) {
                    return a.date - b.date
                });
                break;
        }

        this.setState({
            worklogs: worklogs,
            sorting: type
        })
    }

    deleteWorkLog(id) {
        let worklogs = [...this.state.worklogs];
        let index = worklogs.findIndex(function (worklog) {
            return worklog.id === id;
        });

        worklogs.splice(index, 1);
        this.setState(function (state) {
            return {worklogs: worklogs}
        })
    }

    editWorkLog(worklog) {
        let worklogs = [...this.state.worklogs];
        let index = worklogs.findIndex(function (_worklog) {
            return _worklog.id === worklog.id;
        });

        let edited_worklog = {
            ...worklog
        };

        worklogs[index] = edited_worklog;
        this.setState({worklogs: worklogs});
    }

    getProjectDurationAggregate() {
        let project_hours = PROJECT_LIST.map(function (project) {
            return {name: project, duration: 0}
        });

        project_hours.forEach(function (projectObj) {
            try {
                projectObj.duration = this.state.worklogs.filter(function (worklog) {
                    if (worklog.project === projectObj.name) {
                        return true
                    }
                }).map(function (worklog) {
                    return worklog.duration
                }).reduce(function (total, num) {
                    return total + num
                });
            } catch (e) {
                projectObj.duration = 0;
            }
        }.bind(this));

        return project_hours
    }

    getTotalDurationToday() {
        let hours = this.state.worklogs.filter(function (worklog) {
            let d = new Date();
            return worklog.date.toDateString() === d.toDateString();
        }).map(function (worklog) {
            return worklog.duration
        });
        try {
            return hours.reduce(function (total, num) {
                return total + num;
            })
        } catch (e) {
            return 0
        }

    }

    showMore(event) {
        event.preventDefault();
        this.setState(function (state, props) {
            return {list_size: state.list_size + 4}
        })
    }

    componentDidMount() {
        this.sortWorklogs(this.state.sorting)
    }


    render() {
        let projects = this.getProjectDurationAggregate();
        let totalDuration = this.getTotalDurationToday();
        console.log(projects);


        return (
            <div className="App">
                <header className="App-header">
                    <h1>
                        Time Logger
                    </h1>
                </header>
                <LogForm
                    createWorkLog={this.createWorkLog}
                    project_options={[HYDRA, DRAGON_P2P, DRAGON_MY]}/>
                <DailySummary
                    projects={projects}
                    totalDuration={totalDuration}
                />
                <WorkLogs
                    deleteHandler={this.deleteWorkLog}
                    initializeSorting={() => this.sortWorklogs(this.state.sorting)}
                    editHandler={this.editWorkLog}
                    project_options={[HYDRA, DRAGON_P2P, DRAGON_MY]}
                    worklogs={this.state.worklogs.slice(0, this.state.list_size)}>
                    <SortingMenu sorting={this.state.sorting} sortWorklogs={this.sortWorklogs}/>
                </WorkLogs>
                <ShowMore worklogs={this.state.worklogs}
                          showMore={this.showMore}
                          list_size={this.state.list_size}
                />
            </div>
        );
    }
}

export default App;
