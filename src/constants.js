export const HYDRA = 'Hydra';
export const DRAGON_MY = 'Dragon MY';
export const DRAGON_P2P = 'Dragon P2P';

export const DEFAULT = 'DEFAULT';
export const DURATION = 'DURATION';
export const DURATION_REVERSE = 'DURATION_REVERSE';
export const PROJECT = 'PROJECT';
export const PROJECT_REVERSE = 'PROJECT_REVERSE';
