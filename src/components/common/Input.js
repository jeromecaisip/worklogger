import React from 'react';


let TextInput = ({onChange, name, label, value, type, ...props} = {type: 'text'}) => {
    let inputHTML;
    if (type === 'number') {
        inputHTML = <input
            type={type}
            min="1"
            value={value}
            name={name}
            onChange={onChange}/>
    } else {
        inputHTML = <input
            type={type}
            minLength="1"
            name={name}
            value={value}
            onChange={onChange}/>
    }

    if (label) {
        return (
            <span>
        <label htmlFor={name}>{label}</label>
                {inputHTML}
        </span>
        )
    } else {
        return (
            inputHTML
        )
    }
};

let Select = ({options, onChange, name, label,value, ...props}) => {
    return (
        <span>
            {label ? <label name={name}> {label}</label> : null}
            <select
                value={value}
                onChange={onChange}>
            {options.map((option, index) => <option key={index} value={option}>{option}</option>)}
        </select>
        </span>
    )
};


export {
    TextInput,
    Select,
}
