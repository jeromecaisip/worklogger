import React from 'react'

let GenericButton = function ({name,onClick,type,...props}={type:'submit'}) {
   return (
      <button
          onClick={onClick}
          type={type} >{name}</button>
   )
};


export {
    GenericButton
}
