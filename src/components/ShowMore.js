import React from "react";

export default (props) => {
    let {list_size, worklogs, showMore} = props;
    return (
        <div>
            <p>Showing {list_size > worklogs.length ? worklogs.length : list_size} out
                of {worklogs.length} entries.</p>

            {list_size >= worklogs.length ? null :
                <a onClick={showMore} href="#">Show more</a>}

        </div>
    )
}
