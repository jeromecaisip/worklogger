import React from "react";
import {validateLength, validateMinimumHours} from "./utils";
import {WithOptions} from "./LogForm";
import {Select, TextInput} from "./common/Input";
import {GenericButton} from "./common/Button";

export default class Worklog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            update_view: false,
            form_value: {
                project: this.props.worklog.project,
                duration: this.props.worklog.duration,
                remarks: this.props.worklog.remarks
            }
        };
        this.updateHandler = this.updateHandler.bind(this);
        this.changeView = this.changeView.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler(name, value) {
        let form_value = {...this.state.form_value};
        if (name === 'remarks') {
            form_value.remarks = value;
        } else if (name === 'duration') {
            form_value.duration = Number(value);
        } else if (name === 'project') {
            form_value.project = value;
        } else {
            return
        }


        this.setState((state, props) => {
            return {form_value: form_value}
        });
    }

    updateHandler() {
        let worklog = {
            ...this.props.worklog,
            ...this.state.form_value,
        };
        let error_fields = [];
        if (!validateMinimumHours(this.state.form_value.duration)) {
            error_fields = [...error_fields, 'duration']
        }

        if (!validateLength(this.state.form_value.remarks)) {
            error_fields = [...error_fields, 'remarks']
        }

        if (error_fields.length > 0) {
            alert('Please fill in with valid values.')
        } else {
            this.props.editHandler(worklog)
            this.changeView()
        }

    }

    changeView() {
        this.setState(function (state, props) {
            return {update_view: !state.update_view}
        })
    }

    render() {
        let SelectWithOptions = WithOptions(this.props.project_options)(Select);
        let read_html = <tr>
            <td>{this.props.worklog.duration}h</td>
            <td>{this.props.worklog.project}</td>
            <td>{this.props.worklog.remarks}</td>
            <td>
                <GenericButton
                    onClick={this.changeView}
                    type={"button"}
                    name="Edit"/>
                <GenericButton
                    onClick={() => this.props.deleteHandler(this.props.worklog.id)}
                    type={"button"}
                    name="Delete"/>
            </td>
        </tr>;
        let update_html = <tr>
            <td><TextInput type="number"
                           onChange={(event) => this.changeHandler('duration', event.target.value)}
                           value={this.state.form_value.duration}/></td>
            <td><SelectWithOptions
                onChange={(event) => this.changeHandler('project', event.target.value)}
                value={this.state.form_value.project}
            /></td>
            <td><TextInput
                onChange={(event) => this.changeHandler('remarks', event.target.value)}
                value={this.state.form_value.remarks}
                type="text"/></td>
            <td>
                <GenericButton
                    onClick={this.updateHandler}
                    type={"button"}
                    name="Save"/>
                <GenericButton
                    onClick={this.changeView}
                    type={"button"}
                    name="Cancel"/>
            </td>
        </tr>;

        return (
            this.state.update_view ? update_html : read_html
        )
    }
}





