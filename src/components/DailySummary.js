import React from 'react'


let DailySummary = function (props) {
    let {projects, totalDuration} = props;
    return (
        <div>
            <h3>Daily Summary</h3>
            <table>
                <tbody>
                {projects.map((project, index) => {
                    return (
                        <tr key={index}>
                            <td>{project.name}</td>
                            <td>{project.duration}h</td>
                        </tr>
                    )
                })}
                <tr>
                    <td>TOTAL LOG FOR THE DAY :</td>
                    <td>{totalDuration}</td>
                </tr>
                </tbody>
            </table>
        </div>
    )
};

export {
    DailySummary
}
