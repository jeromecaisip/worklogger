export function validateLength(value) {
    if (value.length <= 0) {
        return false
    } else {
        return true
    }
}

export function validateMinimumHours(value) {
    if (value <= 0) {
        return false
    } else {
        return true
    }
}
