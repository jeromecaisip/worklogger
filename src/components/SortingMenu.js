import {GenericButton} from "./common/Button";
import React from "react";
import {DEFAULT, DURATION, DURATION_REVERSE, PROJECT, PROJECT_REVERSE} from "../constants";

export default (props) => {
    let {sorting, sortWorklogs} = props;
    let ProjectSortButton;
    let DurationSortButton;

    if (sorting === PROJECT) {
        ProjectSortButton = <GenericButton name="Project ^" onClick={() => {
            sortWorklogs(DEFAULT);
            sortWorklogs(PROJECT_REVERSE)
        }}/>
    } else if (sorting === PROJECT_REVERSE) {
        ProjectSortButton = <GenericButton name="Project v" onClick={() => sortWorklogs(DEFAULT)}/>
    } else {
        ProjectSortButton = <GenericButton name="Project" onClick={() => {
            sortWorklogs(DEFAULT);
            sortWorklogs(PROJECT)
        }}/>
    }

    if (sorting === DURATION) {
        DurationSortButton = <GenericButton name="Duration ^" onClick={() => {
            sortWorklogs(DEFAULT);
            sortWorklogs(DURATION_REVERSE)
        }}/>
    } else if (sorting === DURATION_REVERSE) {
        DurationSortButton = <GenericButton name="Duration v" onClick={() => sortWorklogs(DEFAULT)}/>
    } else {
        DurationSortButton = <GenericButton name="Duration " onClick={() => {
            sortWorklogs(DEFAULT);
            sortWorklogs(DURATION)
        }}/>
    }
    return (
        <p>Sort list by: {DurationSortButton} {ProjectSortButton} <GenericButton name="Reset"
                                                                                 onClick={() => this.sortWorklogs(DEFAULT)}/>
        </p>
    )
}
