import React from "react";
import {Select, TextInput} from "./common/Input";
import {GenericButton} from "./common/Button";
import {HYDRA} from "../constants";
import {validateLength,validateMinimumHours} from "./utils";


export let WithOptions = function (options) {
    return function (Component) {
        return function (props) {
            return <Component options={options} {...props} />
        }
    }
};


export default class LogForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form_value: {
                project: HYDRA,
                duration: '',
                remarks: '',
            }
        };
        this.submitHandler = this.submitHandler.bind(this)
    }


    changeHandler(name, value) {
        let form_value = {...this.state.form_value};
        if (name === 'remarks') {
            form_value.remarks = value;
        } else if (name === 'duration') {
            form_value.duration = value;
        } else if (name === 'project') {
            form_value.project = value;
        } else {
            return
        }

        this.setState((state, props) => {
            return {form_value: form_value}
        });
    }

    submitHandler(event) {
        event.preventDefault();
        let error_fields = [];
        if (!validateMinimumHours(this.state.form_value.duration)) {
            error_fields = [...error_fields, 'duration']
        }

        if (!validateLength(this.state.form_value.remarks)) {
            error_fields = [...error_fields, 'remarks']
        }

        if (error_fields.length > 0) {
            alert('Please fill in with valid values.')
        } else {
            this.props.createWorkLog(
                this.state.form_value.duration,
                this.state.form_value.project,
                this.state.form_value.remarks
            );

            this.setState(function (state, props) {
                return {
                    form_value: {
                        project: HYDRA,
                        duration: '',
                        remarks: ''
                    }
                }
            })
        }
    }


    render() {
        let SelectWithOptions = WithOptions(this.props.project_options)(Select);
        return (
            <div>
                <h3>Add Time Log</h3>
                <form onSubmit={this.submitHandler}>
                    <TextInput
                        label="Duration"
                        name="duration"
                        type="number"
                        value={this.state.form_value.duration}
                        onChange={(event) => this.changeHandler('duration', event.target.value)}
                    />
                    <SelectWithOptions
                        label="Project"
                        name="project"
                        value={this.state.form_value.project}
                        onChange={(event) => this.changeHandler('project', event.target.value)}
                        options={this.props.project_options}/>
                    <TextInput
                        label="Remarks"
                        name="remarks"
                        value={this.state.form_value.remarks}
                        onChange={(event) => this.changeHandler('remarks', event.target.value)}
                    />

                    <GenericButton name="Add" onClick={this.submitHandler}/>
                </form>
            </div>
        )
    }
}

