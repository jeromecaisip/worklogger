import React from 'react';
import Worklog from './Worklog'

let WorkLogs = function (props) {
    let {worklogs, deleteHandler, project_options, editHandler} = props;
    return (
        <div>
            <h3>Time Log Records</h3>
            {props.children}
            <table>
                <tbody>
                <tr>
                    <th>DURATION</th>
                    <th>PROJECT</th>
                    <th>REMARKS</th>
                    <th>ACTIONS</th>
                </tr>
                {
                    worklogs.map(function (worklog) {
                        return (
                            <Worklog
                                key={worklog.id}
                                worklog={worklog}
                                editHandler={editHandler}
                                deleteHandler={deleteHandler}
                                project_options={project_options}
                            />
                        )
                    })
                }
                </tbody>
            </table>
        </div>
    )
};

export {
    WorkLogs
}
